﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCSong.Lib.Block
{
    public class BlockChange
    {
        private Player p;
        private Level level;

        public void PlaceBlock(byte block, ushort x, ushort y, ushort z)
        {
            if (block == 0) block = 1;
            level.Blockchange(p, x, y, z, block);
        }

        public void PlaceLiquid(byte liquid, ushort x, ushort y, ushort z)
        {
            List<byte> liquidblocks = new List<byte>() { 195, 194, 193, 190, 191, 146, 145, 160, 8, 9 };
            foreach (byte blocks in liquidblocks)
            {
                if (liquid != blocks)
                {
                    liquid = 9;
                }
                else
                {
                    level.Blockchange(p, x, y, z, liquid);
                }
            }
        }

        public void ExplodePlayer(Player target, int size)
        {
            if (size == 0) size = 1;
            level.MakeExplosion(target.pos[0], target.pos[1], target.pos[2], size);
        }

    }
}

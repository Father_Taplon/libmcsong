﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCSong.Lib.AllAffected
{
    public class All
    {
        private Player p;
        private Level level;

        /// <summary>
        /// Apply the command specified to every player on the server 
        /// Note: I did this the lazy way, don't use this yet!
        /// </summary>
        /// <param name="action"></param>
        /// <param name="parameter"></param>
        public void AllGet(string action, string parameter)
        {
            Player.players.ForEach(delegate(Player plrs)
            {
                Command.all.Find(action).Use(plrs, parameter);
            });
        }

        /// <summary>
        /// Make all players use the command specified in the first parameter
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameter"></param>
        public void AllUse(string command, string parameter = "")
        {
            Player.players.ForEach(delegate(Player plrs)
            {
                Command.all.Find(command).Use(plrs, parameter);
            });
        }

    }
}

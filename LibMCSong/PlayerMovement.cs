﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MCSong.Lib.Movement
{
    public class PlayerMovement
    {
        private Player p;
        private Level level;

        public void GoForward(Player target, int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                target.SendPos(0, target.pos[0], target.pos[1], (ushort)(target.pos[2] + 1), target.rot[0], target.rot[1]);
            }
        }
        public void GoBackward(Player target, int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                target.SendPos(0, target.pos[0], target.pos[1], (ushort)(target.pos[2] - 1), target.rot[0], target.rot[1]);
            }
        }
        public void GoLeft(Player target, int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                target.SendPos(0, (ushort)(target.pos[0] - 1), target.pos[1], target.pos[2], target.rot[0], target.rot[1]);
            }
        }
        public void GoRight(Player target, int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                target.SendPos(0, (ushort)(target.pos[0] + 1), target.pos[1], target.pos[2], target.rot[0], target.rot[1]);
            }
        }


        public void NegativeRotate(Player target, byte value, int times)
        {
            if (value == 0) { unchecked { value = (byte)-25; } }
            if (times == 0) { times = 6; }
            else
            {

                for (int i = 0; i < times; i++)
                {
                    unchecked { target.SendPos((byte)-1, target.pos[0], target.pos[1], target.pos[2], (byte)(target.rot[0] / value), target.rot[1]); }
                }
            }  
        }

        public void PositiveRotate(Player target, byte value, int times)
        {
            if (value == 0) { value = 25; }
            if (times == 0) { times = 6; }
            else
            {
                for (int i = 0; i < times; i++)
                {
                    unchecked { target.SendPos((byte)-1, target.pos[0], target.pos[1], target.pos[2], (byte)(target.rot[0] * value), target.rot[1]); }
                }
            }    
        }
    }
}
